import router from './router'
import express from 'express'

const app: express.Application = express()

app.use(router)

export default app
