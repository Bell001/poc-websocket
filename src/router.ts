import express from 'express'
import { getResult } from './controller'

const router = express.Router()

router.get('/', getResult)

export default router
